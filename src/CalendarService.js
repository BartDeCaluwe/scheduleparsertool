// https://developers.google.com/calendar/v3/reference/
const CALENDAR_API_URL = 'https://www.googleapis.com/calendar/v3'

export default {
  data() {
    return {
      calendarId: localStorage.getItem('preferences') ? JSON.parse(localStorage.getItem('preferences')).calendar : null
    }
  },
  methods: {
    async getCalendarList() {
      const url = `${CALENDAR_API_URL}/users/me/calendarList`
      const res = await this.$http.get(url);
      return res.data;
    },
    async createEvent({summary, end, start}) {
      const url = `${CALENDAR_API_URL}/calendars/${this.calendarId}/events`
      const body = {
        summary,
        end,
        start
      }
      const res = await this.$http.post(url,  body )
      return res.data;
    }
  }
}
