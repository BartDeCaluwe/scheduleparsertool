import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import Buefy from 'buefy';
import axios from 'axios';
import Authentication from './components/Authentication'

Vue.prototype.$http = axios;

Vue.use(Buefy);
Vue.mixin(Authentication);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
}).$mount('#app');
